from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# URL de conecxão do banco
SQLALCHEMY_DATABASE_URL = (
    # Aqui vai a string de conexão, ex: "mysql+mysqlconnector://<user>:<password>@<hoast:port>/<database_name>"
)

# create_engine é o inicio de toda comunicação com as APIs do banco de dados (DBAPI)
engine = create_engine(SQLALCHEMY_DATABASE_URL)

# Futura atual sessão de conecxão com o banco de dados
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

"""
Antes de começarmos a trocar informação com o banco, é preciso que haja uma
configuração expecífica que permite a descrição das tabelas que estaremos lidando,
para então haja um mapeamento das nossas classes para essas tabelas.

No SQLAlchemy essas duas tarefas são performadas juntas em um sistema chamado "declarative",
que nos permite o banco de dados que será mapeado.
"""
Base = declarative_base()

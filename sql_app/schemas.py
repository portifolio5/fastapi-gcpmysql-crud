from typing import List, Optional

from pydantic import BaseModel


class SchemasItemBase(BaseModel):
    title: str
    description: Optional[str] = None


class SchemasItemCreate(SchemasItemBase):
    pass


class SchemasItem(SchemasItemBase):
    id: int
    owner_id: int
    """
    Configuração para o pydantic: com essa classe de configuração, o pydantic
    se torna compatível com ORMs, além de validar notação de classe (acesso de
    atributo usando ponto [.]), e de permitir que possamos usar os esquemas no
    "response_model" da requisição.
    """

    class Config:
        orm_mode = True


class SchemasUserBase(BaseModel):
    email: str


class SchemasUserCreate(SchemasUserBase):
    password: str


class SchemasUser(SchemasUserBase):
    id: int
    is_active: bool
    items: List[SchemasItem] = []

    class Config:
        orm_mode = True
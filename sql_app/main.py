from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from crud import *
from schemas import *
from models import Base

from database import SessionLocal, engine

"""
create_all é um método que orientam as consultas; primeiro é checado
se a tabela de consulta existe, caso não, ela cria no banco de dados
a tabela.
"""
Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/users/", response_model=List[SchemasUser])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud_get_users(db, skip=skip, limit=limit)
    return users


@app.post("/users/", response_model=SchemasUser)
def create_user(user: SchemasUserCreate, db: Session = Depends(get_db)):
    db_user = crud_get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud_create_user(db=db, user=user)


@app.get("/users/count")
def count_users(db: Session = Depends(get_db)):
    return crud_get_user_count(db)


@app.get("/users/{user_id}", response_model=SchemasUser)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud_get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.post("/users/{user_id}/items/", response_model=SchemasItem)
def create_item_for_user(
    user_id: int, item: SchemasItemCreate, db: Session = Depends(get_db)
):
    return crud_create_user_item(db=db, item=item, user_id=user_id)


@app.get("/items/", response_model=List[SchemasItem])
def read_items(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    items = crud_get_items(db, skip=skip, limit=limit)
    return items

from sqlalchemy.orm import Session

from models import ModelsUser, ModelsItem  # Modelos para fazer a consulta
from schemas import SchemasItemCreate, SchemasUserCreate  # Schemas para o request body


def crud_get_user_count(db: Session):
    return db.query(ModelsUser).count()


def crud_get_user(db: Session):
    return db.query(ModelsUser).filter(ModelsUser.id == user_id).first()


def crud_get_user_by_email(db: Session, email: str):
    return db.query(ModelsUser).filter(ModelsUser.email == email).first()


def crud_get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(ModelsUser).offset(skip).limit(limit).all()


def crud_create_user(db: Session, user: SchemasUserCreate):
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = ModelsUser(email=user.email, hashed_password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def crud_get_items(db: Session, skip: int = 0, limit: int = 100):
    return db.query(ModelsItem).offset(skip).limit(limit).all()


def crud_create_user_item(db: Session, item: SchemasItemCreate, user_id: int):
    db_item = ModelsItem(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

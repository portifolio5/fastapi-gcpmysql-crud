from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from database import Base


class ModelsUser(Base):
    __tablename__ = "users"  # conta ao sqlalchemy o nome da tabela a usar no banco para cada um desses modelos

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String(length=50), unique=True, index=True)
    hashed_password = Column(String(length=100))
    is_active = Column(Boolean, default=True)

    items = relationship("ModelsItem", back_populates="owner")


"""
OBS: O parâmetro back_populates liga os objetos 
em uma relação bidirecional,onde o contrário do 
típo de relação é verdadeiro.

Por exemplo:
A relação entre a classe "User" e
"Item" é one-to-many bidirecional, sendo bidirecional
devido a ambos possuirem uma relationship() e que possuem
o atributo "back_populates" referenciando cada relação e vice-versa.
"""


class ModelsItem(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(length=50), index=True)
    description = Column(String(length=80), index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("ModelsUser", back_populates="items")
